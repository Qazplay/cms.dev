-- phpMyAdmin SQL Dump
-- version 4.4.14.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 16, 2017 at 07:09 PM
-- Server version: 5.7.13
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms.dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `is_hidden`, `name`, `label`) VALUES
(1, 0, 'User', 'Пользователь');

-- --------------------------------------------------------

--
-- Table structure for table `types_attributes`
--

CREATE TABLE IF NOT EXISTS `types_attributes` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `kind` varchar(255) NOT NULL,
  `settings` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types_attributes`
--

INSERT INTO `types_attributes` (`id`, `type`, `name`, `label`, `kind`, `settings`) VALUES
(1, 'User', 'created_at', 'Date of creation', 'DATETIME', '{"translatable":0,"interface":{"required":1,"visible":1,"sort":3}}'),
(2, 'User', 'name', 'User name', 'STRING', '{"translatable":0,"interface":{"required":1,"visible":1,"sort":0}}'),
(3, 'User', 'email', 'User email', 'STRING', '{"translatable":0,"interface":{"required":1,"visible":1,"sort":1}}'),
(4, 'User', 'password', 'User password', 'PASSWORD', '{"translatable":0,"interface":{"required":0,"visible":0,"sort":2}}'),
(5, 'User', 'remember_token', 'Remember token', 'STRING', '{"translatable":0,"interface":{"required":0,"visible":0,"sort":4}}');

-- --------------------------------------------------------

--
-- Table structure for table `type_user_subjects`
--

CREATE TABLE IF NOT EXISTS `type_user_subjects` (
  `id` bigint(20) NOT NULL,
  `is_related` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_user_subjects`
--

INSERT INTO `type_user_subjects` (`id`, `is_related`, `created_at`, `name`, `email`, `password`, `remember_token`) VALUES
(1, 0, '2017-07-22 21:00:51', 'Kazplay', 'director@artmedia.kz', 'dd124278ad30db8d8fb44cda9912bab5861d8c7d', '1e043fea3ebb9765dcb623f1fe2c730910741c4b'),
(2, 0, '2017-08-16 15:25:24', 'Slava', 'slava@artmedia.kz', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL),
(3, 0, '2017-08-16 14:25:41', 'Olesya', 'olesya@artmedia.kz', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_name` (`name`) USING BTREE;

--
-- Indexes for table `types_attributes`
--
ALTER TABLE `types_attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_type_name` (`type`,`name`) USING BTREE,
  ADD KEY `type` (`type`) USING BTREE;

--
-- Indexes for table `type_user_subjects`
--
ALTER TABLE `type_user_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `is_related` (`is_related`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `types_attributes`
--
ALTER TABLE `types_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `type_user_subjects`
--
ALTER TABLE `type_user_subjects`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
