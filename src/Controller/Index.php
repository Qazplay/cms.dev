<?php namespace Src\Controller;

use Core\Facade\Template;
use Core\Gateway\Subjects;
use Core\Model\Subject;
use Core\Service\Registry;
use Core\Service\Validator;
use Core\Service\Validator\Email;
use Core\Service\Validator\Lambda;
use Core\Service\Validator\NotEmpty;
use Src\Gateway\Product;

class Index extends AbstractController
{
    protected function bootRouting()
    {
        $this->GET('/', 'indexPage');
        $this->GET('/subscribe', 'subscribe');
    }

    protected function bootDispatchIndexTemplateDefaults()
    {
        Template::defaults([
            'hits' => (new Product)->getHits(),
            'slider' => $this->slider(),
            'mainPage' => Subjects::of('Page')->find(21)
        ]);
    }

    public function indexPage()
    {
        return $this->html(Template::render('src/index'));
    }

    protected function slider()
    {
        return Template::render('src/slider', [
            'slider' => Subjects::Slider()->with('elements.textblock.link')->find(1)
        ]);
    }

    public function subscribe()
    {
        $rules = [
            'email' => [
                new NotEmpty('Заполните e-mail'),
                new Email('Введите корректный e-mail'),
                new Lambda(function ($value) {
                    return !Subjects::of('EmailBase')->select(['email' => $value])->count();
                }, 'Вы уже зарегистрированы')
            ]
        ];

        if (!($validator = new Validator($rules, Registry::get('http.request.query')))->isValid()) {
            return $this->text(join("\n", $validator->getErrors()));
        }

        Subjects::of('EmailBase')->create($validator->getData());

        return $this->text('ok');
    }
}
