<?php namespace Src\Controller;

use Core\Facade\Template;
use Core\Gateway\Subjects;
use Core\Middleware\Controller;

abstract class AbstractController extends Controller
{
    protected function menu()
    {
        return Template::render('src/menu', [
            'menuElements' => Subjects::of('Menu')->with(['elements.body'])->find(1)->elements->sortBy('sort')
        ]);
    }
}