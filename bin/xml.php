<?php namespace Bin;

set_time_limit(0);
date_default_timezone_set('Asia/Almaty');
chdir(dirname(__DIR__));
require 'vendor/autoload.php';

use Core\Facade\App;
use Core\Gateway\Subjects;
use Src\Model;

class XML
{
    public static function checkAlreadyRun()
    {
        exec('ps x', $out);
        $out = join("\n", $out);
        $file = __FILE__;
        //file_put_contents(ROOT_PATH.'Storage/log.txt', $out);
        if(substr_count($out, $file)>1) return true;

        return false;
    }

    protected static function read($path, callable $handle)
    {
        $files = array_diff(scandir($path), array('..', '.'));

        $index = 0;

        foreach ($files as $file) {

            $file = $path.'/'.$file;

            if (is_dir($file)) {
                return self::read($file, $handle);
            }

            if (($xml = simplexml_load_file($file)) === false) {
                print_r(libxml_get_errors());
                continue;
            }

            //unlink($file); // save or unlink? do we need file archive ?

            $handle($xml); //die();

            if ($index++ >= 4) return $index;

            usleep(20);
        }

        return $index;
    }

    public static function loadOrders()
    {
        self::read('obmen/orders/in', function ($xml) {

            if (!($client = Subjects::Client(['phone' => (string) $xml->order->mobile])->first())) return false; // no client for order

            ($order = new Model\Order([
                'mobile' => (string) $xml->order->mobile,
                'number' => (string) $xml->order->number,
                'route' => (string) $xml->order->route,
                'gruz' => (string) $xml->order->gruz,
                'places' => (string) $xml->order->places,
                'volume' => (string) $xml->order->volume,
                'weight' => (string) $xml->order->weight,
                'status' => (string) $xml->order->status,
                'price' => (string) $xml->order->price,
                'location' => (string) $xml->order->location
            ]))->save();

            $client->orders()->save($order);
        });
    }

    public static function loadClients()
    {
        self::read('obmen/clients/in', function ($xml) {

            if (Subjects::Client(['phone' => (string) $xml->client->mobile])->count()) return false; // already exists

            $clientData = [
                'phone' => (string) $xml->client->mobile,
                'email' => (string) $xml->client->email,
            ];

            if (($type = (string) $xml->client['type']) == 'fizlico') {
                /*<?xml version="1.0" encoding="windows-1251"?>
                <doc>
                  <client type="fizlico">
                    <name>Ибрагимов Нуржан Ташходжаевич</name>
                    <country>Казахстан</country>
                    <mobile>+77713222218</mobile>
                    <email></email>
                    <other_phones>+77004411246</other_phones>
                    <city>Шымкент</city>
                    <address>Республика Казахстан, г. Шымкент, ул. Утегенова дом 7030</address>
                    <iin_bin></iin_bin>
                    <doc></doc>
                    <doc_number>11</doc_number>
                    <doc_date>2000-11-20</doc_date>
                    <doc_vydal>МЮ РК</doc_vydal>
                    <doc_url></doc_url>
                  </client>
                </doc>*/

                $clientData['client_type'] = 'ClientIndividual';

                ($type = new Model\ClientIndividual([
                    'name' => (string) $xml->client->name,
                    'mobile' => (string) $xml->client->mobile,
                    'other_phones' => (string) $xml->client->other_phones,
                    'email' => (string) $xml->client->email,
                    'country' => (string) $xml->client->country,
                    'city' => (string) $xml->client->city,
                    'address' => (string) $xml->client->address,
                    'doc' => (string) $xml->client->doc,
                    'doc_number' => (string) $xml->client->doc_number,
                    'doc_date' => (string) $xml->client->doc_date,
                    'doc_vydal' => (string) $xml->client->doc_vydal,
                    'doc_url' => (string) $xml->client->doc_url,
                ]))->save();

            } else if ($type == 'urlico') {
                /*<?xml version="1.0" encoding="windows-1251"?>
                <doc>
                  <client type="urlico">
                    <name>BROCKMULLER TRUCKING</name>
                    <country>Казахстан</country>
                    <mobile>+77775522484</mobile>
                    <email>irina.strelnikova@gw-world.com</email>
                    <other_phones>+77272393444;+ 7 777 552 24 84; + 7 701 982 36 86</other_phones>
                    <city>Алматы</city>
                    <address>Республика Казахстан 050054 г. Алматы, ул. Майлина 79</address>
                    <iin_bin>120 340 000 817</iin_bin>
                    <ur_address>Республика Казахстан 050054 г. Алматы, ул. Майлина 79</ur_address>
                    <forma>ТОО</forma>
                    <face></face>
                    <site>www.almaty.de</site>
                    <iik>KZ259 143 989 14BC 33112 (KZT)</iik>
                    <bank>Дочерний Банк Акционерное Общество «Сбербанк России»</bank>
                    <bik>SABRKZKA</bik>
                    <okpo></okpo>
                    <kor_schet></kor_schet>
                    <inn></inn>
                    <kpp></kpp>
                    <ogrn></ogrn>
                  </client>
                </doc>*/

                $clientData['client_type'] = 'ClientFirm';

                ($type = new Model\ClientFirm([
                    'name' => (string) $xml->client->name,
                    'country' => (string) $xml->client->country,
                    'city' => (string) $xml->client->city,
                    'address' => (string) $xml->client->address,
                    'ur_address' => (string) $xml->client->ur_address,
                    'mobile' => (string) $xml->client->mobile,
                    'other_phones' => (string) $xml->client->other_phones,
                    'email' => (string) $xml->client->email,
                    'iin_bin' => (string) $xml->client->iin_bin,
                    'forma' => (string) $xml->client->forma,
                    'site' => (string) $xml->client->site,
                    'iik' => (string) $xml->client->iik,
                    'bank' => (string) $xml->client->bank,
                    'bik' => (string) $xml->client->bik,
                    'okpo' => (string) $xml->client->okpo,
                    'kor_schet' => (string) $xml->client->kor_schet,
                    'inn' => (string) $xml->client->inn,
                    'kpp' => (string) $xml->client->kpp,
                    'ogrn' => (string) $xml->client->ogrn,
                ]))->save();
            }

            ($client = new Model\Client($clientData))->save();

            $client->client_type()->save($type);

            /*if (!empty($phones = (string) $xml->client->other_phones)) {
                if (strstr($phones, ';')) {
                    $phones = explode(';', $phones);
                } else if (strstr($phones, ',')) {
                    $phones = explode(',', $phones);
                }

                if (is_array($phones)) {
                    $phonesArray = [];

                    foreach($phones as $phone) {
                        $phonesArray [] = ['phone' => $phone];
                    }

                    $type->other_phones()->saveMany(Subjects::of('Phone')->createMany($phonesArray));
                } else {
                    $type->other_phones()->save(Subjects::of('Phone')->create(['phone' => $phones]));
                }
            }*/
        });
    }
}

if(XML::checkAlreadyRun()) die();

App::init(require 'config/config.php');

libxml_use_internal_errors(true);

echo 'Clients loaded: ' . XML::loadClients() . "\n";

echo 'Orders loaded: ' . XML::loadOrders() . "\n";